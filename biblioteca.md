BIBLIOTECA

- Dos roles, bibliotecario, lector
    - Bibliotecario administra libros y espacios
    - Usuario ve información de libros

- Préstamos, lo realiza el bibliotecario
- Los lectores ven sus prestamos activos
- Los lectores pueden crear una lista de deseos (libros que quieren)
