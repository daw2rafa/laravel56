RESERVAS:
- Roles usuario y administrador
- Aplicación para reservar recursos

- Recursos:
    - Admin CRUD completo
    - User lista y detalle del recurso

- Reserva
    - CRUD completo por administrador y usuario propietario
    - usuario no propietario: ver pero no editar.


BIBLIOTECA

- Dos roles, bibliotecario, lector
    - Bibliotecario administra libros y espacios
    - Usuario ve información de libros

- Préstamos, lo realiza el bibliotecario
- Los lectores ven sus prestamos activos
- Los lectores pueden crear una lista de deseos (libros que quieren)
